winpty \
  docker run --rm -it --name ng_dev \
    --mount type=bind,source="$(pwd)",target=/home/app -w //home//app \
    -e "PORT=4200" -p 8080:4200 \
     ng_dev //bin//bash
