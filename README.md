# Docker Development

Getting started files for creating docker images for local develpment.

There is nothing special in these scripts, I just hadn't found a single resource that put it all together
for me when I was starting out.

The intention is that these scripts will be downloaded, then customised for your own use.

Current setups for vue and angular.

## Usage

Pattern for Vue
1. cd vue
2. build.sh
3. run.sh 

## Extra

### 1. winpty
If developing in git-bash, check extra->git_bash_run_vue.sh for an example.
Of note, winpty and using // over /, in _some_ situations


